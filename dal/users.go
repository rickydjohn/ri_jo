package dal

import "bitbucket.org/ri_jo/logger"

// getUser is an unexported function to build the user's profile
func (d *dal) getUser(uid int, log *logger.Log) (User, error) {
	var u User

	query := "select uid, first_name, last_name, email, address, phone, registration_type from users where uid=?"
	if err := d.db.QueryRow(query, uid).Scan(&u.UID, &u.FName, &u.LName, &u.Email, &u.Address, &u.Phone, &u.RegistrationType); err != nil {
		log.Error(err.Error())
		return u, err
	}

	return u, nil
}
