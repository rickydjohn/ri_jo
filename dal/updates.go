package dal

import (
	"bitbucket.org/ri_jo/logger"
)

// UpdateUser updates the user's profile
func (d *dal) UpdateUser(u User, log *logger.Log) (User, error) {
	tx, err := d.db.Begin()
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	defer tx.Rollback()
	user, err := d.getUser(u.UID, log)
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	if user.RegistrationType == "user" {
		if _, err := tx.Exec("update users set first_name=?, last_name=?, email=?, address=?, phone=? where uid=?", u.FName, u.LName, u.Email, u.Address, u.Phone, u.UID); err != nil {
			log.Error(err)
			return User{}, err
		}
	} else {
		if _, err := tx.Exec("update users set first_name=?, last_name=?, address=?, phone=? where uid=?", u.FName, u.LName, u.Address, u.Phone, u.UID); err != nil {
			log.Error(err)
			return User{}, err
		}
	}
	tx.Commit()

	usr, err := d.GetProfile(u.UID, log)
	if err != nil {
		return User{}, err
	}
	return usr, nil
}
