package dal

import "bitbucket.org/ri_jo/logger"

// CreateUser creates a user entry
func (d *dal) CreateUser(u User, password string, log *logger.Log) (User, error) {
	tx, err := d.db.Begin()
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	defer tx.Rollback()
	result, err := tx.Exec("insert into users(first_name, last_name, email,address,  phone, registration_type) values(?, ?, ?, ?, ?, ?)", u.FName, u.LName, u.Email, u.Address, u.Phone, u.RegistrationType)
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	if u.RegistrationType == "user" {
		if _, err := tx.Exec("insert into auth(uid, password) values(?, password(?))", id, password); err != nil {
			log.Error(err)
			return User{}, err
		}
	}

	tx.Commit()
	user, err := d.GetProfile(int(id), log)
	if err != nil {
		log.Error(err)
		return User{}, err
	}
	return user, nil
}
