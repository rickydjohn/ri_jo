package dal

import (
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/ri_jo/logger"
)

func randstr(l int) string {
	rand.Seed(time.Now().UnixNano())
	az := []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"}
	var str string
	for i := 0; i < l; i++ {
		str += az[rand.Intn(len(az)-1)]
	}
	return str
}

func randyear() time.Time { return time.Now().AddDate(rand.Intn(20), rand.Intn(12), rand.Intn(28)) }

func TestLogin(t *testing.T) {
	c := Config{"nseparser", "megh", "localhost", "demoapp"}
	log := logger.New()
	d, err := New(c, log)
	if err != nil {
		log.Error(err)
	}

	u := User{
		FName:            randstr(10),
		LName:            randstr(10),
		Email:            "test_" + randstr(5) + "@" + randstr(6) + ".com",
		Address:          randstr(100),
		Phone:            randstr(9),
		RegistrationType: "user",
	}

	log.Info("creating user....")
	log.Info(u)
	password := randstr(10)
	p, err := d.CreateUser(u, password, log)
	if err != nil {
		log.Panic(err)
	}

	log.Info("Updated Profile")
	newP, err := d.GetProfile(p.UID, log)
	if err != nil {
		log.Panic(err)
	}
	newP.FName = randstr(12)
	newP.Address = randstr(50)
	np, err := d.UpdateUser(newP, log)
	if err != nil {
		log.Panic(err)
	}

	log.Info("Final Profile")
	log.Info(np)

	log.Info("logging in")
	_, err = d.Login(np.Email, password, log)
	if err != nil {
		t.Error(err)
	}

}
