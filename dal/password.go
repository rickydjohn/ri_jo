package dal

import (
	"fmt"

	"bitbucket.org/ri_jo/logger"
)

func (d *dal) Passlinksave(email, link string, log *logger.Log) error {
	usr, _ := d.GetUserByEmail(email, log)
	tx, err := d.db.Begin()
	if err != nil {
		log.Info(err)
		return err
	}
	defer tx.Rollback()
	if _, err := tx.Exec("insert into password_reset(uid, link) values(?, ?)", usr.UID, link); err != nil {
		log.Info(err)
		return err
	}
	tx.Commit()
	return nil
}

func (d *dal) UserFromLink(link string, log *logger.Log) (User, error) {
	var uid int
	if err := d.db.QueryRow("select uid from password_reset where link=? and accessed=false", link).Scan(&uid); err != nil {
		log.Error(err)
		return User{}, err
	}
	fmt.Println(" getting user ..............", uid)

	tx, err := d.db.Begin()
	if err != nil {
		return User{}, err
	}
	defer tx.Rollback()

	if _, err := tx.Exec("update password_reset set accessed=true where link=? and uid=?", link, uid); err != nil {
		log.Error(err)
		return User{}, err
	}
	tx.Commit()
	return d.getUser(uid, log)
}

func (d *dal) SetPwd(id int, pwd string) error {
	tx, err := d.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()
	if _, err := tx.Exec("update auth set password=password(?) where uid=?", pwd, id); err != nil {
		return err
	}
	tx.Commit()
	return nil
}
