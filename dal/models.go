package dal

import (
	"database/sql"
	"time"

	"bitbucket.org/ri_jo/logger"
)

type Config struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Database string `json:"database"`
}

type User struct {
	UID              int       `db:"id" json:"uid,omitempty"` // user id
	FName            string    `db:"fist_name" json:"fName"`
	LName            string    `db:"last_name" json:"lName"`
	Email            string    `db:"email" json:"email"`
	Address          string    `db:"address" json:"address"`
	Phone            string    `db:"phone" json:"phone"`
	RegistrationType string    `db:"registration_type" json"registrationType,omitempty"`
	CreatedAt        time.Time `db:"created_at,omitempty"`
	UpdatedAt        time.Time `db:"updated_at,omitempty"`
}

type Pwd interface {
	Passlinksave(email, link string, log *logger.Log) error
	UserFromLink(link string, log *logger.Log) (User, error)
	SetPwd(id int, pwd string) error
}

type DB interface {
	Login(email, pwd string, log *logger.Log) (User, error)
	GetProfile(id int, log *logger.Log) (User, error)
	UpdateUser(u User, log *logger.Log) (User, error)
	CreateUser(u User, password string, log *logger.Log) (User, error)
	GetUserByEmail(email string, log *logger.Log) (User, error)
	Pwd
}

type dal struct {
	db  *sql.DB
	log *logger.Log
}
