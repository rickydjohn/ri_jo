package dal

import (
	"database/sql"

	"bitbucket.org/ri_jo/logger"
	_ "github.com/go-sql-driver/mysql"
)

//GetProfile builds user profile by calling supporting methods
func (d *dal) GetProfile(id int, log *logger.Log) (User, error) {
	user, err := d.getUser(id, log)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

// Login generates a user profile and returns
func (d *dal) Login(email, pwd string, log *logger.Log) (User, error) {
	var uid int
	query := "select u.uid from users u join auth a on u.uid=a.uid where u.email=? and a.password=password(?)"
	if err := d.db.QueryRow(query, email, pwd).Scan(&uid); err != nil {
		log.Error(err.Error())
		return User{}, err
	}
	user, err := d.GetProfile(uid, log)
	if err != nil {
		log.Error(err.Error())
		return User{}, err
	}
	return user, nil
}

func (d *dal) GetUserByEmail(email string, log *logger.Log) (User, error) {
	var id int
	if err := d.db.QueryRow("select uid from users where email=?", email).Scan(&id); err != nil {
		log.Error(err.Error())
		return User{}, err
	}
	user, err := d.GetProfile(id, log)
	if err != nil {
		log.Error(err.Error())
		return User{}, err
	}
	return user, nil

}

// New creating a db object and returning an interface
func New(c Config, log *logger.Log) (DB, error) {
	db, err := sql.Open("mysql", c.Username+":"+c.Password+"@tcp("+c.Host+":3306)/"+c.Database+"?parseTime=true")
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}
	log.Info("Successfully initiated database connection...")
	return &dal{
		db:  db,
		log: log,
	}, nil

}
