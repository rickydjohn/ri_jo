package sessions

import (
	"testing"

	"bitbucket.org/ri_jo/logger"
)

func TestSessions(t *testing.T) {
	l := logger.New()
	l.AddFields("module", "sessions test")

	news := New(l)
	uuid, err := news.Create(110, "test@test.com")
	if err != nil {
		t.Fatal(err)
	}

	l.Info("new session created with" + uuid)

	if news.Validate(uuid) {
		l.Info("Validation completed...")
	}
	id, email, err := news.UserDetail(uuid)
	if err != nil {
		t.Fatal(err)
	}
	l.Info("retrieved user info")
	l.Info(id)
	l.Info(email)

	if err := news.Delete(uuid); err != nil {
		t.Error(err)
	}
	l.Info("Completed testing")
}
