package sessions

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"

	"bitbucket.org/ri_jo/logger"
)

type user struct {
	UID   int
	Email string
}

type Session struct {
	active map[string]user
	log    *logger.Log
}

var (
	ErrSessionNotFound error = errors.New("Not an active session")
)

func New(log *logger.Log) *Session {
	log.Info("Start sessions manager..")
	return &Session{
		active: make(map[string]user),
		log:    log,
	}
}

func (s *Session) randstring() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:]), nil
}

// create session
func (s *Session) Create(uid int, email string) (string, error) {
	s.log.Info("creating new session ID for " + email)
	id, err := s.randstring()
	if err != nil {
		return "", err
	}

	if _, k := s.active[id]; k { // uniqueness is not guaranteed here hence the recursion
		s.Create(uid, email)
	}

	s.active[id] = user{UID: uid, Email: email}
	return id, nil
}

// Validate the session
func (s *Session) Validate(uuid string) bool {
	if _, k := s.active[uuid]; k {
		return true
	}
	return false
}

// Delete a session
func (s *Session) Delete(uuid string) error {
	if !s.Validate(uuid) {
		return ErrSessionNotFound
	}
	delete(s.active, uuid)
	return nil
}

// UserDetail return session owners detail
func (s *Session) UserDetail(uuid string) (int, string, error) {
	if s.Validate(uuid) {
		d, _ := s.active[uuid]
		return d.UID, d.Email, nil
	}
	return 0, "", ErrSessionNotFound
}

func (s *Session) GetAllSession() ([]byte, error) {
	bt, err := json.Marshal(s.active)
	if err != nil {
		return nil, err
	}
	return bt, err
}
