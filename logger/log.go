package logger

import (
	"encoding/json"
	"log"
	"os"
)

type Log struct {
	prefix map[string][]byte
	log    *log.Logger
}

// Info outputs info entry to stderr. Nothing fancy
func (n *Log) Info(data interface{}) {
	var prefixes string
	for i, v := range n.prefix {
		prefixes += i + ":" + string(v) + ", "
	}
	if len(prefixes) > 0 {
		n.log.Printf("[INFO] TAGS: %s MESSAGE: %v", prefixes, data)
		return
	}
	n.log.Printf("[INFO] MESSAGE: %v", data)
}

// Error tries to print out the data with error tag
func (n *Log) Error(data interface{}) {
	var prefixes string
	for i, v := range n.prefix {
		prefixes += i + ":" + string(v) + ", "
	}
	if len(prefixes) > 0 {
		n.log.Printf("[ERROR] TAGS: %s MESSAGE: %v ", prefixes, data)
		return
	}
	n.log.Printf("[ERROR] MESSAGE: %v ", data)
}

// Panic does what it does best
func (n *Log) Panic(data interface{}) {
	var prefixes string
	for i, v := range n.prefix {
		prefixes += i + ":" + string(v) + ", "
	}
	if len(prefixes) > 0 {
		n.log.Fatalf("[PANIC] TAGS: %s MESSAGE: %v", prefixes, data)
		return
	}
	n.log.Fatalf("[PANIC] MESSAGE: %v", data)
}

// AddFields will take a key value pair and print it as part of the log
func (n *Log) AddFields(key string, value interface{}) {
	bt, err := json.Marshal(value)
	if err != nil {
		n.Error(err.Error())
	}
	n.prefix[key] = bt
}

// NewLogger wipes the slate clean and returns a logger with the original stderr file
func (n *Log) NewLogger(key string, value interface{}) *Log {
	l := &Log{
		log:    n.log,
		prefix: make(map[string][]byte),
	}
	l.AddFields(key, value)
	return l
}

// New returns a logger (a humble logger)
//TODO: expose logger directly which will expose additional functionalities of logger
// This is bad because logging at flag 4+ wont work the way its supposed to.
func New() *Log {
	logger := log.New(os.Stderr, "", 0) // write logs to stderr. start pricess using systemd and capture the logs in journalctl
	return &Log{
		prefix: make(map[string][]byte),
		log:    logger,
	}
}
