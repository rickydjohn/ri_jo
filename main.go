package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"

	"bitbucket.org/ri_jo/dal"
	"bitbucket.org/ri_jo/logger"
	"bitbucket.org/ri_jo/router"
	"bitbucket.org/ri_jo/sessions"
)

func main() {
	log := logger.New()
	log.AddFields("module", "main")
	var cnf struct {
		Database    dal.Config         `json:"database"`
		Application router.Application `json:"application"`
	}
	config := flag.String("c", "", "configuration file")
	flag.Parse()

	if *config == "" {
		flag.PrintDefaults()
	}

	bt, err := ioutil.ReadFile(*config)
	if err != nil {
		log.Panic(err.Error())
	}
	if err := json.Unmarshal(bt, &cnf); err != nil {
		log.Panic(err.Error())
	}
	db, err := dal.New(cnf.Database, log.NewLogger("module", "DAL"))
	if err != nil {
		log.Panic(err.Error())
	}
	session := sessions.New(log.NewLogger("module", "Session"))
	rtr := router.New(cnf.Application, db, session, log.NewLogger("module", "Router"))
	rtr.Begin()
}
