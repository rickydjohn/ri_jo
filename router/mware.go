package router

import (
	"context"
	"crypto/rand"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/ri_jo/logger"
)

func randstring() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:]), nil
}

func (r *App) mwbasic(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		start := time.Now()

		log := logger.New()
		log.AddFields("remoteIP", req.RemoteAddr)
		log.AddFields("method", req.Method)
		log.AddFields("route", req.URL.Path)
		reqID, _ := randstring()
		log.AddFields("reqID", reqID)
		ctx := context.WithValue(req.Context(), "log", log)
		req = req.WithContext(ctx)
		logger := ctx.Value("log").(*logger.Log)
		logger.Info("Handling new request")
		f(rw, req)
		log.Info("completed handling request in " + time.Since(start).String())
	}
}

func (r *App) mwAuthC(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		log, err := loggerFromContext(req)
		if err != nil {
			http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
			return
		}
		log.Info("checking if user is authenticated")
		cookie, err := req.Cookie("ssid")
		if r.session.Validate(cookie.Value) {
			f(rw, req)
		} else {
			log.Error("cookie is not from an active session. Redirecting..")
			http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
		}
	}
}

func (r *App) mwAuthH(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		log, err := loggerFromContext(req)
		if err != nil {
			http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
			return
		}
		log.Info("checking if user is authenticated")
		ssid := req.Header.Get("ssid")
		if r.session.Validate(ssid) {
			f(rw, req)
		} else {
			log.Error("Header is not valid. Redirecting..")
			http.Error(rw, "invalid header", http.StatusUnauthorized)
			return
		}
	}
}
