package router

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/ri_jo/dal"
	"github.com/gorilla/mux"
)

func (a *App) getsessions(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Add("content-type", "application/json")
	bt, err := a.session.GetAllSession()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	rw.WriteHeader(http.StatusOK)
	rw.Write(bt)
	return
}

func (a *App) updateUser(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	log, err := loggerFromContext(req)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	bt, err := ioutil.ReadAll(req.Body)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	var data dal.User
	if err := json.Unmarshal(bt, &data); err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	uid, err := strconv.Atoi(vars["id"])
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	data.UID = uid
	if _, err := a.db.UpdateUser(data, log); err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	rw.WriteHeader(http.StatusOK)
	return
}

func (a *App) createUser(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	log.Info("creating new user")
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	var data login
	bt, err := ioutil.ReadAll(req.Body)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	if err := json.Unmarshal(bt, &data); err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}

	user, _ := a.db.CreateUser(dal.User{Email: data.Username}, data.Password, log)
	sessId, err := a.session.Create(user.UID, user.Email)
	if err != nil {
		log.Error(err)
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}
	rw.Header().Add("ssid", sessId)
	http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: sessId, MaxAge: 86400})
	bt, _ = json.Marshal(user)
	rw.Write(bt)
	return
}

func (a *App) resetpwd(rw http.ResponseWriter, req *http.Request) {
	var userpwd struct {
		UID      string `json:"uid"`
		Password string `json:"password"`
	}

	bt, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.Unmarshal(bt, &userpwd); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	uid, _ := strconv.Atoi(userpwd.UID)
	if err := a.db.SetPwd(uid, userpwd.Password); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	return
}

func (a *App) createuser(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	var user struct {
		User     dal.User `json:"user"`
		Password string   `json:"password"`
	}

	bt, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	defer req.Body.Close()
	if err := json.Unmarshal(bt, &user); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	user.User.RegistrationType = "user"
	u, err := a.db.CreateUser(user.User, user.Password, log)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	byt, err := json.Marshal(u)

	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	rw.Write(byt)
	return
}
