package router

import (
	"net/http"
	"text/template"

	"bitbucket.org/ri_jo/dal"
	"bitbucket.org/ri_jo/logger"
	"bitbucket.org/ri_jo/sessions"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type App struct {
	cnf      Application
	db       dal.DB
	log      *logger.Log
	session  *sessions.Session
	handler  *mux.Router
	template *template.Template
	gauth    *oauth2.Config
}

func New(cnf Application, db dal.DB, s *sessions.Session, log *logger.Log) *App {
	log.Info("Creating a router object")
	t := template.Must(template.ParseGlob("web/static/*.html"))
	handler := mux.NewRouter().StrictSlash(true) // using gorilla took kit because the doc said it is okay to do so
	r := &App{
		cnf:      cnf,
		db:       db,
		log:      log,
		session:  s,
		handler:  handler,
		template: t,
		gauth: &oauth2.Config{
			RedirectURL:  cnf.Callback,
			ClientID:     cnf.ClientID,
			ClientSecret: cnf.Secret,
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"},
			Endpoint:     google.Endpoint,
		},
	}

	fs := http.FileServer(http.Dir("."))
	handler.PathPrefix("/web").Handler(fs)

	// front end
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.index)).Path("/")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.signup)).Path("/signup")
	handler.Methods("POST").HandlerFunc(r.mwbasic(r.login)).Path("/login")
	handler.Methods("POST").HandlerFunc(r.mwbasic(r.register)).Path("/register")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.mwAuthC(r.login))).Path("/home")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.mwAuthC(r.edit))).Path("/edit")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.logout)).Path("/logout")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.forgotpassword)).Path("/forgotpassword")
	handler.Methods("POST").HandlerFunc(r.mwbasic(r.resetform)).Path("/resetform")

	// gauth support
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.glogin)).Path("/glogin")
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.callback)).Path("/callback")

	// pwd handle
	handler.Methods("GET").HandlerFunc(r.mwbasic(r.pwdreset)).Path("/reset_pwd/{link}")

	// API
	api := handler.PathPrefix("/api").Subrouter()
	api.Methods("GET").HandlerFunc(r.mwbasic(r.getsessions)).Path("/sessions/all")
	api.Methods("POST").HandlerFunc(r.mwbasic(r.createuser)).Path("/user")
	api.Methods("PUT").HandlerFunc(r.mwbasic(r.updateUser)).Path("/user/{id:[0-9]+}")
	api.Methods("POST").HandlerFunc(r.mwbasic(r.resetpwd)).Path("/resetpwd")

	return r
}

func (r *App) Begin() error {
	defer func() {
		if err := recover(); err != nil {
			r.log.Error(err)
		}
	}()
	if err := http.ListenAndServe(r.cnf.Listen, r.handler); err != nil {
		r.log.Error(err)
		return err
	}
	return nil
}
