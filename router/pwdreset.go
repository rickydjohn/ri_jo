package router

import (
	"database/sql"
	"encoding/base64"
	"net/http"
	"time"

	"bitbucket.org/ri_jo/logger"
	"github.com/gorilla/mux"
)

func (r *App) forgotpassword(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An Internal error occurred."})
		return
	}
	r.template.ExecuteTemplate(rw, "passreset.html", webdata{})
	return
}

func (r *App) resetform(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An Internal error occurred."})
		return
	}
	email := req.FormValue("email")
	user, err := r.db.GetUserByEmail(email, log)
	if err == sql.ErrNoRows {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Username does not exist in our database"})
		return
	} else if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	if user.RegistrationType == "gauth" {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "User registered using Google Authentication. Use Google to sign in."})
		return
	}

	if err := r.buildPasswordReset(user.Email, log); err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "User registered using Google Authentication. Use Google to sign in."})
		return
	}

	r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An email has been to " + user.Email})
	return
}

func (r *App) buildPasswordReset(email string, log *logger.Log) error {

	randstring := base64.StdEncoding.EncodeToString([]byte(time.Now().String()))
	if err := r.db.Passlinksave(email, randstring, log); err != nil {
		return err
	}

	if err := r.mailpwdlink(email, randstring); err != nil {
		return err
	}
	return nil
}

func (r *App) pwdreset(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An Internal error occurred."})
		return
	}
	vars := mux.Vars(req)
	pwdlink := vars["link"]
	if pwdlink == "" {
		log.Error("invalid password reset link")
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "invalid password reset link"})
		return
	}
	user, err := r.db.UserFromLink(pwdlink, log)
	if err == sql.ErrNoRows {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Expired password reset link"})
		return
	}
	r.template.ExecuteTemplate(rw, "resetui.html", webdata{User: user})
	return
}
