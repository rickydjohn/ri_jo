package router

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"bitbucket.org/ri_jo/dal"
	"golang.org/x/oauth2"
)

var (
	randstr = "random"
)

func (r *App) glogin(rw http.ResponseWriter, req *http.Request) {
	url := r.gauth.AuthCodeURL(randstr)
	http.Redirect(rw, req, url, http.StatusTemporaryRedirect)
}

func (r *App) callback(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	if req.FormValue("state") != randstr {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Unable to login with google auth"})
		return
	}
	token, err := r.gauth.Exchange(oauth2.NoContext, req.FormValue("code"))
	if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}

	client := r.gauth.Client(oauth2.NoContext, token)
	guser, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	bt, err := ioutil.ReadAll(guser.Body)
	if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}

	var auth struct {
		Email       string `json:"email"`
		Given_name  string `json:"given_name"`
		Family_name string `json:"family_name"`
	}

	if err := json.Unmarshal(bt, &auth); err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}

	u, err := r.db.GetUserByEmail(auth.Email, log)
	if err == sql.ErrNoRows {
		// add db check with email
		user, err := r.db.CreateUser(dal.User{Email: auth.Email, FName: auth.Given_name, LName: auth.Family_name, RegistrationType: "gauth"}, "", log)

		if err != nil {
			rw.WriteHeader(http.StatusTemporaryRedirect)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		sessId, err := r.session.Create(user.UID, user.Email)
		if err != nil {
			log.Error(err)
			rw.WriteHeader(http.StatusInternalServerError)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		rw.Header().Add("ssid", sessId)
		http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: sessId, MaxAge: 86400})
		r.template.ExecuteTemplate(rw, "edit.html", webdata{User: user})
		return
	} else if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	sessId, err := r.session.Create(u.UID, u.Email)
	if err != nil {
		log.Error(err)
		rw.WriteHeader(http.StatusInternalServerError)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	rw.Header().Add("ssid", sessId)
	http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: sessId, MaxAge: 86400})
	r.template.ExecuteTemplate(rw, "profile.html", webdata{User: u})
	return

}
