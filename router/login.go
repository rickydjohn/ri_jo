package router

import (
	"database/sql"
	"fmt"
	"net/http"
)

func (r *App) login(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An Internal error occurred."})
		return
	}
	log.Info("logging in user")
	uname := req.FormValue("email")
	passwrd := req.FormValue("password")

	user, err := r.db.Login(uname, passwrd, log)
	if err == sql.ErrNoRows {
		log.Error(err)
		rw.WriteHeader(http.StatusUnauthorized)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Incorrect username/password"})
		return
	} else if err != nil {
		log.Error(err)
		rw.WriteHeader(http.StatusUnauthorized)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	sessId, err := r.session.Create(user.UID, user.Email)
	if err != nil {
		log.Error(err)
		rw.WriteHeader(http.StatusInternalServerError)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	rw.Header().Add("ssid", sessId)
	http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: sessId, MaxAge: 86400})
	r.template.ExecuteTemplate(rw, "profile.html", webdata{User: user})
	return
}

//logout
func (r *App) logout(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	cookie, err := req.Cookie("ssid")
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "User is not logged in. Redirecting to homepage"})
		return
	}
	r.session.Delete(cookie.String())
	rw.Header().Del("ssid")
	http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: "", MaxAge: -1})
	rw.WriteHeader(http.StatusTemporaryRedirect)
	r.template.ExecuteTemplate(rw, "index.html", webdata{})
	return
}

// edit
func (r *App) edit(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "An Internal error occurred."})
		return
	}
	cookie, err := req.Cookie("ssid")
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "User is not logged in. Redirecting to homepage"})
		return
	}
	user, _, err := r.session.UserDetail(cookie.Value)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Internal error.Unable to complete task"})
		return
	}
	prof, err := r.db.GetProfile(user, log)
	if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Internal error.Unable to complete task"})
		return
	}
	fmt.Println(prof)
	if err := r.template.ExecuteTemplate(rw, "edit.html", webdata{User: prof}); err != nil {
		log.Error(err)
		http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
		return
	}
}
