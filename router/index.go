package router

import (
	"database/sql"
	"fmt"
	"net/http"

	"bitbucket.org/ri_jo/dal"
)

func (r *App) index(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Info("starting new log")
	cookie, err := req.Cookie("ssid")
	if err == http.ErrNoCookie {
		log.Info("Session cookie is unavailable.")
		if err := r.template.ExecuteTemplate(rw, "index.html", webdata{}); err != nil {
			log.Error(err)
			return
		}
	} else if !r.session.Validate(cookie.Value) {
		// not a valid session -- route back to index
		fmt.Println("COOKIE ERROR", r.session.Validate(cookie.String()))
		log.Info("not a valid cookie. Routing to index page")
		if err := r.template.ExecuteTemplate(rw, "index.html", webdata{}); err != nil {
			log.Error(err)
			rw.WriteHeader(http.StatusTemporaryRedirect)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "Invalid session. Redirecting"})
			return
		}
	} else {
		// route to home page
		id, _, err := r.session.UserDetail(cookie.Value)
		if err != nil {
			log.Error(err)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		profile, err := r.db.GetProfile(id, log)
		if err != nil {
			log.Error(err)
			rw.WriteHeader(http.StatusInternalServerError)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		log.Info("user logged in with session ID " + cookie.String())
		r.template.ExecuteTemplate(rw, "profile.html", webdata{User: profile})
		return
	}
}

// sign up

func (r *App) signup(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Info("registering user")
	if err := r.template.ExecuteTemplate(rw, "signup.html", webdata{}); err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
}

// register new user

func (r *App) register(rw http.ResponseWriter, req *http.Request) {
	log, err := loggerFromContext(req)
	if err != nil {
		rw.WriteHeader(http.StatusTemporaryRedirect)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	uname := req.FormValue("email")
	passwrd := req.FormValue("password")

	_, err = r.db.GetUserByEmail(uname, log)
	if err == sql.ErrNoRows {
		user, err := r.db.CreateUser(dal.User{Email: uname, RegistrationType: "user"}, passwrd, log)

		if err != nil {
			rw.WriteHeader(http.StatusTemporaryRedirect)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		sessId, err := r.session.Create(user.UID, user.Email)
		if err != nil {
			log.Error(err)
			rw.WriteHeader(http.StatusInternalServerError)
			r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
			return
		}
		rw.Header().Add("ssid", sessId)
		http.SetCookie(rw, &http.Cookie{Name: "ssid", Value: sessId, MaxAge: 86400})
		r.template.ExecuteTemplate(rw, "edit.html", webdata{User: user})
		return

	} else if err != nil {
		log.Error(err)
		r.template.ExecuteTemplate(rw, "index.html", webdata{Message: err.Error()})
		return
	}
	rw.WriteHeader(http.StatusTemporaryRedirect)
	r.template.ExecuteTemplate(rw, "index.html", webdata{Message: "User exist already. Sign In.."})
	return
}
