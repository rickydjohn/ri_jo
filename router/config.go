package router

import (
	"bitbucket.org/ri_jo/dal"
)

type Application struct {
	Listen   string `json:"listen"`
	Name     string `json:"name"`
	ClientID string `json:"clientID"`
	Secret   string `json:"secret"`
	Callback string `json:"callback"`
	Email    string `json:"email"`
	Password string `json:"password"`
	SMTP     string `json:"smtp"`
	PublicIp string `json:"publicIp"`
}

type login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type webdata struct {
	User    dal.User
	Message string
}
