package router

import (
	"bytes"
	"errors"
	"fmt"
	"mime/quotedprintable"
	"net/http"
	"net/smtp"

	"bitbucket.org/ri_jo/logger"
)

func loggerFromContext(req *http.Request) (*logger.Log, error) {
	v, k := req.Context().Value("log").(*logger.Log)
	if !k {
		return nil, errors.New("logging object not found in context")
	}
	return v, nil
}

func (r *App) mailpwdlink(email, url string) error {
	auth := smtp.PlainAuth("", r.cnf.Email, r.cnf.Password, "smtp.gmail.com")
	header := make(map[string]string)
	header["From"] = r.cnf.Email
	header["To"] = email
	header["Subject"] = "demoapp password reset link"

	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
	header["Content-Disposition"] = "inline"
	header["Content-Transfer-Encoding"] = "quoted-printable"

	headermsg := ""
	for key, value := range header {
		headermsg += fmt.Sprintf("%s: %s\r\n", key, value)
	}
	pwdlink := r.cnf.PublicIp + "/reset_pwd/" + url
	body := "<p>click <a href='" + pwdlink + "'>here</a> to reset your password<p>"
	var body_message bytes.Buffer
	temp := quotedprintable.NewWriter(&body_message)
	temp.Write([]byte(body))
	temp.Close()

	final_message := headermsg + "\r\n" + body_message.String()

	if err := smtp.SendMail(r.cnf.SMTP, auth, r.cnf.Email, []string{email}, []byte(final_message)); err != nil {
		return err
	}
	return nil
}
