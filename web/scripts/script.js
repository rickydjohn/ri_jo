
function update(){
	data = Array.from(document.querySelectorAll('#myForm input'));
	let obj = {};
	for (i of data) {
		obj[i.name] = i.value
	}

	url = "/api/user/"+obj["id"]
	delete obj.id
	let ndata  = JSON.stringify(obj);

	fetch(url, {
		method:'put',
		body: ndata,
		credentials: 'include'
	}).then(function(response) {
		location.href = "/";
	})
}


function register(){
	data = Array.from(document.querySelectorAll('#myForm input'));
	let obj = {};
	for (i of data) {
		obj[i.name] = i.value
	}
	if (obj.passConfirm != obj.password){
		alert("passwords does not match");
		return;
	}

	if (obj.password == "" ) {
		alert("password cannot be empty");
		return;
	}
}


function resetpwd() {
	data = Array.from(document.querySelectorAll('#myForm input'));
	let obj = {};
	for (i of data) {
		obj[i.name] = i.value
	}
	if (obj.passwordconf != obj.password){
		alert("passwords does not match");
		return;
	}

	if (obj.password == "" ) {
		alert("password cannot be empty");
		return;
	}
	delete obj.passwordconf 
	fetch("/api/resetpwd", {
		method: 'post',
		body: JSON.stringify(obj),
		credentials: 'include'
	}).then (function (response) {
		location.href="/";
	})


}
